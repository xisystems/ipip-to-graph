//GET LIST OF CONSTRUCTS FOR A GIVEN SCALE
MATCH (s:Scale {shortName: "BIG5"})-[c:CONTRUCT]-(i)
WITH COLLECT(c.construct) AS cts
UNWIND cts as ct
WITH DISTINCT ct
RETURN collect(ct) AS constructs

//GET ALL NODES AND RELATIONS TO A SCALE
MATCH (s:Scale {shortName: "BIG5"})-[c:CONTRUCT]-(i)
RETURN s,c,i
LIMIT 3000

//GET ALL NODES TO A SCALE WHERE THE KEYED RELATION TO ITEM IS NEGATIVE
MATCH (s:Scale {shortName: "BIG5"})-[k:KEY {key: -1}]-(i)
RETURN i,k,s
LIMIT 25

//LIST OF ALL SCALES
MATCH (s:Scale)
RETURN collect(s.shortName) as scales, count(s.shortName) as amount

//LIST A SCALE WITH A NAMED CONSTRUCT
MATCH (s:Scale {shortName: "BIG5"})-[c:CONTRUCT {construct: "Emotional Stability"}]-(i)
RETURN i,c,s
LIMIT 300