# IPIP Collect tools


Execute each tool from the ipip-cd directory. Each tool generates a specific file with the composition of XIDs in your /tmp/ folder.

Run for:


- Instruments / Keys / Alpha and Construct
```
ipip-instr
```

- IPIP Items
```
ipip-items
```

- IPIP Labels, Constructs and Instruments
```
ipip-scale
```
