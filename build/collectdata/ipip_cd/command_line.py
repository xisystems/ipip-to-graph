import sys, getopt,argparse,datetime
from ipip_cd.collect_ipip import run as CollectIPIP
from ipip_cd.collect_instrument_keyed_labels import run as CollectINST
from ipip_cd.collect_scale_labels_instruments import run as CollectSCAL


class DefaultHelpParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('Error: %s\n' % message)
        self.print_help()
        sys.exit(2)


def main_CollectIPIP():  
    parser = argparse.ArgumentParser(add_help=True, formatter_class=argparse.MetavarTypeHelpFormatter)
    parser = argparse.ArgumentParser(description='ipip-cd')
    parser.add_argument("-o", "--out", required=True, help="Output folder")
    
    DefaultHelpParser(parser.parse_args())                
    args = vars(parser.parse_args())
    
    CollectIPIP()

def main_CollectINST():  
    parser = argparse.ArgumentParser(add_help=True, formatter_class=argparse.MetavarTypeHelpFormatter)
    parser = argparse.ArgumentParser(description='ipip-cd')
    parser.add_argument("-o", "--out", required=True, help="Output folder")
    
    DefaultHelpParser(parser.parse_args())                
    args = vars(parser.parse_args())
    
    CollectINST()

def main_CollectSCAL():  
    parser = argparse.ArgumentParser(add_help=True, formatter_class=argparse.MetavarTypeHelpFormatter)
    parser = argparse.ArgumentParser(description='ipip-cd')
    parser.add_argument("-o", "--out", required=True, help="Output folder")
    
    DefaultHelpParser(parser.parse_args())                
    args = vars(parser.parse_args())
    
    CollectSCAL()
