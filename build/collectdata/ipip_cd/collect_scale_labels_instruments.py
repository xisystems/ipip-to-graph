import requests,csv,re,uuid
from bs4 import BeautifulSoup


def run():

    IPIPItems=[]
    src01="https://ipip.ori.org/newIndexofScaleLabels.htm"

    ### Extract SCALE Alias/Author/Reference
    scale_description={}
    out=requests.get(src01).text
    rows = BeautifulSoup(out,features="html.parser")
    # Read Note: (at the bottom of the page)
    tag = rows.select("ul")[0]
    scale_named = tag.select("li")
    for i in scale_named:
        sd=i.text.split("=")    
        scale_description[sd[0].strip()]=sd[1].strip()

    ### Extract the Constuct list
    out=requests.get(src01).text
    rows = BeautifulSoup(out,features="html.parser")
    # Read all construct elements (are H3 items)
    tag = rows.select("h3")[0]
    # Constructs separated by endline Html
    item=str(tag).split("<br/>")

    #list_labels=[]
    list_labels_scales=[]
    #list_scales=[]
    list_construct_manual=[]
    #list
    list_scale_named={}

    # Extract the main construct list
    for i in item:
        constructs=i.replace("\r\n"," ").replace("\t"," ")
        c_parse=BeautifulSoup(constructs,features="html.parser")
        line=" ".join(c_parse.text.strip().replace("\t"," ").split())
        try:
            # Extract the label
            rex_construct=re.compile(r'^(?P<construct>.*?)\(')
            label=re.findall(rex_construct,line)[0].strip()
            # Extract the ipip scales between (...)
            rex=re.compile(r'\((.*?)\)')
            ipip_scales_grp=re.findall(rex,line)
            
            # Create UUID based on the label
            xid_uuid_label=str(uuid.uuid5(uuid.NAMESPACE_DNS, 'ipip.xi.systems/#'+label))
            
            # Extract ipip_scales_grp
            for ips in ipip_scales_grp:

                
                # Expand IPIP Scale and Constucts related (NEO: N1) --> NEO: N1, NEO            
                ipip_scales=ips.split(":")            
                #if (len(ipip_scales)==2):            
                ipip_scale_named=ipip_scales[0].strip()
                xid_uuid_scale=str(uuid.uuid5(uuid.NAMESPACE_DNS, 'ipip.xi.systems/#'+ips))            
                xid_uuid_scale_name=str(uuid.uuid5(uuid.NAMESPACE_DNS, 'ipip.xi.systems/#'+ipip_scale_named))
                xid_uuid_scale_instrument_label=str(uuid.uuid5(uuid.NAMESPACE_DNS, 'ipip.xi.systems/#'+ipip_scale_named+label))

                
                if not ipip_scale_named in scale_description:
                    scale_descr=ipip_scale_named
                    author="NOT SET"
                else: 
                    ipip_source_descr=scale_description[ipip_scale_named]            
                    scale_descr_rgx=re.compile(r'^(?P<description>.*?)\(')
                    scale_descr=re.findall(scale_descr_rgx,ipip_source_descr)[0].strip()
                    author_rgx=re.compile(r'\((.*?)\)')
                    author=re.findall(author_rgx,ipip_source_descr)[0].strip()
                    # Store row
                
                list_labels_scales.append([xid_uuid_label,label,xid_uuid_scale_instrument_label,xid_uuid_scale,ips,xid_uuid_scale_name,ipip_scale_named,scale_descr,author])        

        except:
            ### Instruments without format, will be added manually
            list_construct_manual.append(line)

    print("-->TO DO MANUALLY:",list_construct_manual)

    with open('/tmp/IPIP_labels_scales.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["xid_ipip_label","ipip_label","xid_ipip_scale_instrument_label","xid_ipip_scale_construct","ipip_scale_construct","xid_ipip_scale_instrument","ipip_scale_instrument","ipip_scale_instrument_descr","ipip_scale_instrument_author"])
        for i in list_labels_scales:
            writer.writerow(i)
