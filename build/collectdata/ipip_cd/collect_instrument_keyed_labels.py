import csv,uuid

def run():
    list_instrument_keyed_labels=[]

    with open("../../../import/IPIP_ItemsToScale.csv","r") as fcsv:
        csv_reader = csv.reader(fcsv, delimiter=',')
        next(csv_reader)
        for row in csv_reader:
            
            xid_ipip_label=str(uuid.uuid5(uuid.NAMESPACE_DNS, 'ipip.xi.systems/#'+row[4]))        
            xid_ipip_item=str(uuid.uuid5(uuid.NAMESPACE_DNS, 'ipip.xi.systems/#'+row[3]))    
            xid_ipip_scale_instrument_label=str(uuid.uuid5(uuid.NAMESPACE_DNS, 'ipip.xi.systems/#'+row[0]+row[4]))
            xid_ipip_scale_instrument=str(uuid.uuid5(uuid.NAMESPACE_DNS, 'ipip.xi.systems/#'+row[0]))
            ipip_scale_instrument=row[0]
            ipip_alpha=row[1]
            ipip_keyed=int(row[2])
            ipip_label=row[4]
            ipip_item=row[3]

            list_instrument_keyed_labels.append([xid_ipip_scale_instrument_label,xid_ipip_scale_instrument,ipip_scale_instrument,ipip_alpha,ipip_keyed,xid_ipip_item,ipip_item,xid_ipip_label,ipip_label])
        
        
        with open('/tmp/IPIP_list_instrument_keyed_labels.csv', 'w') as file:
            writer = csv.writer(file)
            writer.writerow(["xid_ipip_scale_instrument_label","xid_ipip_scale_instrument","ipip_scale_instrument","ipip_alpha","ipip_keyed","xid_ipip_item","ipip_item","xid_ipip_label","ipip_label"])
            for i in list_instrument_keyed_labels:
                writer.writerow(i)


            