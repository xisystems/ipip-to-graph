## Collect routines for the Data Sources and Schemas

import requests,csv,uuid
from bs4 import BeautifulSoup

def run():

    IPIPItems=[]
    src01="https://ipip.ori.org/AlphabeticalItemList.htm"


    out=requests.get(src01).text
    rows = BeautifulSoup(out,features="html.parser")
    tags = rows.select("tr")
    for row in tags:
        if not row.has_attr('colspan'):
            itemlist=row.findAll(attrs={"class" : "MsoNormal"})
            if not itemlist == []:
                ## Elements in two columns
                if len(itemlist)==2:
                    # Col 0 -> item, Col 1 -> suvey code
                    item=itemlist[0].text.replace("\r\n"," ").replace("'","").replace("\"","").strip()
                    surveycode=itemlist[1].text.replace("\r\n"," ").replace("'","").replace("\"","").strip()

                    xid=str(uuid.uuid5(uuid.NAMESPACE_DNS, 'ipip.xi.systems/#'+item))
                
                    IPIPItems.append([xid,item,surveycode])
            
    ## Remove last element (Footer of the table)
    IPIPItems=IPIPItems[:-1]

    # Write CVS file
    with open('/tmp/IPIP_items.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["xid_ipip_item","item","surveycode"])
        for i in IPIPItems:
            writer.writerow(i)


