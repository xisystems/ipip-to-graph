# Setup.py
import os
from setuptools import setup

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
   name='ipip-cd',
   version='1.0',
   description='IPIP Collect Data',
   license="MIT",
   long_description=read('README.md'),
   author='Rene McCaine, Manuel Parra',
   author_email='',
   packages=['ipip_cd'],
   install_requires=['etaprogress','bs4','requests'], 
   entry_points={
        "console_scripts": [
            "ipip-items = ipip_cd.command_line:main_CollectIPIP",
            "ipip-instr = ipip_cd.command_line:main_CollectINST",
            "ipip-scale = ipip_cd.command_line:main_CollectSCAL"
        ]
    }
)