// Load items and xid_ipip_item code
LOAD CSV WITH HEADERS FROM 'file:///IPIP_items.csv' as ROW
MERGE (i:Item {item:ROW.item, xid:ROW.xid_ipip_item});

// Load labels-instruments-constructor and rel
LOAD CSV WITH HEADERS FROM 'file:///IPIP_labels_scales.csv' as ROW
MERGE (d:Domain {name:ROW.ipip_label,xid:ROW.xid_ipip_label})
MERGE (sc:Scale {name:ROW.ipip_scale_construct,xid:ROW.xid_ipip_scale_instrument,description:ROW.ipip_scale_instrument_descr,author:ROW.ipip_scale_instrument_author})
MERGE (sc)-[:HAS_DOMAIN]->(d)

// Load labels and xid_ipip_label code
LOAD CSV WITH HEADERS FROM 'file:///IPIP_list_instrument_keyed_labels.csv' as ROW
MERGE (i:Item {xid:ROW.xid_ipip_item})
MERGE (sc:Scale {xid:ROW.xid_ipip_scale_instrument})
MERGE (d:Domain {xid:ROW.xid_ipip_label})
MERGE (d)-[:HAS_ITEM]->(i)
MERGE (sc)-[:HAS_ITEM {key: toInteger(ROW.ipip_keyed),alpha:ROW.ipip_alpha}]->(i)
