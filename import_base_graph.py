import pandas as pd
from py2neo import Graph, Node, Relationship

data = pd.read_csv("./import/IPIP_ItemsToScaleNoAlpha.csv")

class Item(GraphObject):
    __primarylabel__ = "Item"
    __primarykey__ = "itemId"

    text = property()
    ipip_item_id = property()

    direction = RelatedTo(Scale)
    construct = RelatedTo(Scale)


class Scale(GraphObject):
    __primarylabel__ = "Scale"
    __primarykey__ = "scaleId"

    shortName = Property()
    longName = Property()
    author = Property()

    direction = RelatedFrom("Item", "DIRECTION")
    construct = RelatedFrom("Item", "CONSTRUCT")

graph = Graph("bolt://localhost:7687", auth=("ipip", "ghG%n54$v"))

# graph.delete_all()

