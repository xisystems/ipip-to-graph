LOAD CSV WITH HEADERS FROM 'file:///IPIP_ItemsToScaleNoAlpha.csv' as ROW
MERGE (i:Item {text: ROW.text})
MERGE (s:Scale {shortName: ROW.instrument})
MERGE (i)-[k:KEY {key: toInteger(ROW.key)}]->(s)
MERGE (i)-[c:CONTRUCT {construct: ROW.label}]->(s);