// Load items and xid_ipip_item code
LOAD CSV WITH HEADERS FROM 'file:///IPIP_items.csv' as ROW
MERGE (i:Item {name:ROW.item, xid_ipip_item:ROW.xid_ipip_item,survey_code:ROW.surveycode});

// Load labels-instruments-constructor and rel
LOAD CSV WITH HEADERS FROM 'file:///IPIP_labels_scales.csv' as ROW
MERGE (l:Label {name:ROW.ipip_label,xid_ipip_label:ROW.xid_ipip_label})
MERGE (ins:Instrument {name:ROW.ipip_scale_instrument,xid_ipip_scale_instrument:ROW.xid_ipip_scale_instrument,description:ROW.ipip_scale_instrument_descr,author:ROW.ipip_scale_instrument_author})
MERGE (insc:InstrumentConstruct {name:ROW.ipip_scale_construct,xid_ipip_scale_construct:ROW.xid_ipip_scale_construct,xid_ipip_scale_instrument_label:ROW.xid_ipip_scale_instrument_label})
MERGE (l)-[h_a:HAS_INSTRUMENT]->(ins)
MERGE (ins)-[h_a:HAS_INSTRUMENT_CONSTRUCT]->(insc);



// Load labels and xid_ipip_label code
LOAD CSV WITH HEADERS FROM 'file:///IPIP_list_instrument_keyed_labels.csv' as ROW
MERGE (i:Item {name: ROW.ipip_item,xid_ipip_item:ROW.xid_ipip_item})
MERGE (ins:Instrument {name:ROW.ipip_scale_instrument,xid_ipip_scale_instrument:ROW.xid_ipip_scale_instrument})
MERGE (insc:InstrumentConstruct {xid_ipip_scale_instrument_label:ROW.xid_ipip_scale_instrument_label}) 
MERGE (insc)-[h_i:HAS_ITEMS {key: toInteger(ROW.ipip_keyed),alpha:ROW.ipip_alpha}]->(i)


